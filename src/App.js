import React, { useState } from 'react';

function countInitial(){
  console.log('run function ex 2')
  return 10
}

function App() {
  //Example 1
  // const [count, setCount] = useState(() => {
  //   console.log('oke');
  //   return 5
  // })

  //Example 2
  //Make Funtion in state and connect with countInitial function
  // () => in state :no longer everytime render in component
  const [count, setCount] = useState(() => countInitial())

  function decrementCount(){
    setCount(prevValue => prevValue - 1)
  }

  function incrementCount(){
    setCount(prevCount => prevCount + 1)
  }

  return (
    <>
      <button onClick={decrementCount}>-</button>
        <span>{count}</span>
      <button onClick={incrementCount}>+</button>
    </>
  );
}

export default App;
